import { Component, ErrorHandler } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  captureException,
  captureMessage,
  TraceClassDecorator,
  TraceMethodDecorator,
} from '@sentry/angular';
import { catchError, EMPTY, Observable, tap } from 'rxjs';
import { DataService } from 'src/services/data-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
@TraceClassDecorator()
export class AppComponent {
  title = 'project';
  text = '';
  res = 0;
  num1 = new FormControl('');
  num2 = new FormControl('');

  constructor(private _dataService: DataService) {
    this.text = this._dataService.getMessage();
    this.onErrorLogToSentry(this.num1.valueChanges).subscribe((v) => {
      this.calculateResult(v, this.num2.value);
    });
    this.onErrorLogToSentry(this.num2.valueChanges).subscribe((v) => {
      this.calculateResult(this.num1.value, v);
    });
  }

  @TraceMethodDecorator()
  calculateResult(num1: string | null, num2: string | null) {
    let n1 = parseInt(num1 ? num1 : '0');
    let n2 = parseInt(num2 ? num2 : '0');
    this.res = n1 + n2;
    captureMessage(`Result calculated ${this.res}`);
  }

  onErrorLogToSentry(obs$: Observable<any>) {
    return obs$.pipe(
      catchError((err) => {
        captureException(err);
        return EMPTY;
      })
    );
  }
}
