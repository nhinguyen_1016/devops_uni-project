import { DataService } from 'src/services/data-service';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  //test res = num1 + num2
  //test res = num1 + (-num2)
  //test res = num1 no num2 input
  //test res = num2 no num1 input
  it('#res is 0 after initialization', () => {
    const appComponent = new AppComponent(new DataService());
    expect(appComponent.res).toBe(0);
  });

  it('#res is 7 after 5 + 2', () => {
    // 2+ 5 = 7
    let num1 = '2';
    let num2 = '5';
    let res = 7;
    const appComponent = new AppComponent(new DataService());
    appComponent.num1.setValue(num1);
    appComponent.num2.setValue(num2);

    expect(appComponent.res).toBe(res);
  });
});
