import { DataService } from './data-service';

describe('DataService', () => {
  let _dataService: DataService;

  it('#getMessage should be defined', () => {
    _dataService = new DataService();
    expect(_dataService.getMessage()).toBeDefined();
  });

  it("#getMessage should return 'Hello World'", () => {
    _dataService = new DataService();
    expect(_dataService.getMessage()).toBe('Hello World!');
  });

  it("#getMessage should fit to regular expression including 'lo' ", () => {
    _dataService = new DataService();
    expect(_dataService.getMessage()).toMatch("lo");
  });

});
